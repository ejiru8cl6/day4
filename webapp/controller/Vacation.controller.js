sap.ui.define([
	"sap/ui/core/mvc/Controller",
	'sap/m/MessageToast'
], function(Controller, MessageToast) {
	"use strict";

	return Controller.extend("nnext.iq.Form.controller.Vacation", {
		onInit: function() {
			var ctrl = this;

			//Submit Data
			var Json_VacationStract = new sap.ui.model.json.JSONModel();
			Json_VacationStract.loadData("model/Json_VacationStract.json");
			Json_VacationStract.attachRequestCompleted(function() {
				//唯一碼
				Json_VacationStract.getProperty("/FormData").RequisitionID = "RequisitionID";
				ctrl.getView().setModel(Json_VacationStract, "Json_VacationStract");
			});

			//申請人部門
			var oApplicantDept = new sap.ui.model.json.JSONModel();
			oApplicantDept.loadData("model/Json_Departments.json");
			oApplicantDept.attachRequestCompleted(function() {
				ctrl.getView().setModel(oApplicantDept, "oApplicantDept");
			});

			//申請人
			var oApplicantID = new sap.ui.model.json.JSONModel();
			oApplicantID.loadData("model/Json_Members.json");
			oApplicantID.attachRequestCompleted(function() {
				ctrl.getView().setModel(oApplicantID, "oApplicantID");
			});

			//代理人
			var o_theAgentID = new sap.ui.model.json.JSONModel();
			o_theAgentID.loadData("model/Json_Members.json");
			o_theAgentID.attachRequestCompleted(function() {
				ctrl.getView().setModel(o_theAgentID, "o_theAgentID");
			});

			//假別
			var o_theType = new sap.ui.model.json.JSONModel();
			o_theType.loadData("model/Json_Type.json");
			o_theType.attachRequestCompleted(function() {
				ctrl.getView().setModel(o_theType, "o_theType");
			});

			//時數
			var oTimes = new sap.ui.model.json.JSONModel();
			oTimes.loadData("model/Json_Times.json");
			oTimes.attachRequestCompleted(function() {
				ctrl.getView().setModel(oTimes, "oTimes");
			});

		},

		ondateChange: function(oEvent) {
			var oDP = oEvent.oSource;
			var bValid = oEvent.getParameter("valid");
			if (bValid) {
				oDP.setValueState(sap.ui.core.ValueState.None);
			} else {
				oDP.setValueState(sap.ui.core.ValueState.Error);
			}
		},
		//彈性上班時數是否可填寫
		onFlexible_workChange: function(oEvent) {
			var sBoxStatus = oEvent.getSource().getSelected();
			if (sBoxStatus === true) {
				this.getView().byId('_theTotalHour').setEnabled(true);
			} else {
				this.getView().byId('_theTotalHour').setEnabled(false).setValue("");
			}
		},
		//申請部門改變時
		onDeptChange: function() {
			var ctrl = this;
			var sDeptID = this.getView().byId("ApplicantDept").getSelectedKey();

			//重設申請人
			var oApplicantID = new sap.ui.model.json.JSONModel();
			oApplicantID.loadData("model/Json_Members.json");
			oApplicantID.attachRequestCompleted(function() {
				var oMembersProperty = oApplicantID.getProperty("/");
				var aTmp = [];
				for (var i = 0; i < oMembersProperty.length; i++) {
					if (oMembersProperty[i].departmentId === sDeptID) {
						aTmp.push(oMembersProperty[i]);
					}
				}
				ctrl.getView().getModel("oApplicantID").setProperty("/", aTmp);
			});
		},
		//送出資料
		onSubmit: function() {
			MessageToast.show(JSON.stringify(this.getView().getModel("Json_VacationStract").getProperty("/FormData")));
		}
	});

});